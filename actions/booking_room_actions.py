from datetime import datetime
from typing import Any, Text, Dict, List, Union, Optional
from rasa_sdk import Tracker, Action
from rasa_sdk.events import EventType, SlotSet
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction, REQUESTED_SLOT
from datetime import date
from application.service.service_extractor_ocr import *
from application.data.data_loader import *
from application.service.service_check_infor_checkin import *
from application.service.service_booking_room_template import *
from application.utilities.extract_info_service import InformationExtractor


class BookingRoomForm(FormAction):

    def __init__(self):
        self.sender_id = ""
        self.flag_phone_verifying = False
        self.template_selector = TemplateSelector()
        self.information_extractor = InformationExtractor()

    def name(self) -> Text:
        return "booking_room_form"

    @staticmethod
    def required_slots(tracker: "Tracker") -> List[Text]:
        return [
            "destination",
            "hotel_name",
            "from_date",
            "to_date",
            "type_room",
            "count_room",
            "verify_info_hotel",
            "user_name",
            "phone_number",
            "verify_info_contact"
        ]

    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict[Text, Any]]]]:
        return {'destination': [self.from_entity(entity="destination"), self.from_text()],
                'hotel_name': [self.from_entity(entity="hotel_name"), self.from_text()],
                'from_date': [self.from_entity(entity="from_date"), self.from_text()],
                'to_date': [self.from_entity(entity="to_date"), self.from_text()],
                'type_room': [self.from_entity(entity="type_room"), self.from_text()],
                'count_room': [self.from_entity(entity="count_room"), self.from_text()],
                'verify_info_hotel': [self.from_entity(entity="verify_info_hotel"), self.from_text()],
                'user_name': [self.from_entity(entity="user_name"), self.from_text()],
                'phone_number': [self.from_entity(entity="phone_number"), self.from_text()],
                'verify_info_contact': [self.from_entity(entity="verify_info_contact"), self.from_text()]
                }

    def validate_destination(self, value: "Text", dispatcher: "CollectingDispatcher", tracker: "Tracker",
                             domain: Dict[Text, Any], ) -> Dict[Text, Any]:
        if isinstance(value, str):
            if value in self.template_selector.destinations:
                return {"destination": value}
            else:
                dispatcher.utter_message(
                    text="Hiện tại bên mình đang không có điểm đến tại {}".format(value))

        return {"destination": None}

    def validate_hotel_name(self, value: "Text", dispatcher: "CollectingDispatcher", tracker: "Tracker",
                            domain: Dict[Text, Any], ) -> Dict[Text, Any]:
        if isinstance(value, str):
            if value in self.template_selector.data["hotel_name"]:
                return {"destination": value}
            return {"hotel_name": value}
        return {"hotel_name": None}

    def validate_from_date(self, value: "Text", dispatcher: "CollectingDispatcher", tracker: "Tracker",
                           domain: Dict[Text, Any], ) -> Dict[Text, Any]:
        if isinstance(value, str):
            if self.information_extractor.check_date_format(value):
                datetime_value = datetime.strptime(value, "%d/%m/%Y")
                if datetime_value.date() >= date.today():
                    return {"from_date": value}
                else:
                    dispatcher.utter_message(
                        text="Có vẻ như ngày {} được bạn cung cấp chưa đúng :( ".format(value))
            else:
                dispatcher.utter_message(
                    text="Có vẻ như ngày {} được bạn cung cấp chưa đúng định dạng bên mình hỗ trợ.".format(value))
        return {"from_date": None}

    def validate_to_date(self, value: "Text", dispatcher: "CollectingDispatcher", tracker: "Tracker",
                         domain: Dict[Text, Any], ) -> Dict[Text, Any]:
        if isinstance(value, str):
            if self.information_extractor.check_date_format(value):
                datetime_value = datetime.strptime(value, "%d/%m/%Y")
                datetime_from = datetime.strptime(tracker.get_slot("from_date"), "%d/%m/%Y")
                if datetime_value.date() >= date.today() and datetime_value.date() >= datetime_from.date():
                    return {"from_date": value}
                else:
                    dispatcher.utter_message(
                        text="Có vẻ như ngày {} được bạn cung cấp chưa đúng :( ".format(value))
            else:
                dispatcher.utter_message(
                    text="Có vẻ như ngày {} được bạn cung cấp đúng định dạng bên mình hỗ trợ.".format(value))
        return {"to_date": None}

    def validate_type_room(self, value: "Text", dispatcher: "CollectingDispatcher", tracker: "Tracker",
                           domain: Dict[Text, Any], ) -> Dict[Text, Any]:
        if isinstance(value, str):
            return {"type_room": value}
        return {"type_room": None}

    def validate_count_room(self, value: "Text", dispatcher: "CollectingDispatcher", tracker: "Tracker",
                            domain: Dict[Text, Any], ) -> Dict[Text, Any]:
        if isinstance(value, str):
            if value.isnumeric():
                return {"count_room": value}
            else:
                dispatcher.utter_message(
                    text="Có vẻ như số lượng phòng bạn cung cấp đúng định dạng bên mình hỗ trợ.".format(
                        value))
        return {"count_room": None}

    def validate_verify_info_hotel(self, value: "Text", dispatcher: "CollectingDispatcher", tracker: "Tracker",
                                   domain: Dict[Text, Any], ) -> Dict[Text, Any]:
        if isinstance(value, str):
            intent = tracker.latest_message.get("intent", {}).get("name")
            if intent == 'affirm':
                return {"verify_info_hotel": "True"}
            if value in ["True", "Đúng vậy", "Đúng rồi bạn"]:
                return {"verify_info_hotel": "True"}

        return {"verify_info_hotel": None, "destination": None, "hotel_name": None,
                "from_date": None, "to_date": None, "type_room": None, "count_room": None}

    def validate_user_name(self, value: "Text", dispatcher: "CollectingDispatcher", tracker: "Tracker",
                           domain: Dict[Text, Any], ) -> Dict[Text, Any]:
        if isinstance(value, str):
            return {"user_name": value}
        return {"user_name": None}

    def validate_phone_number(self, value: "Text", dispatcher: "CollectingDispatcher", tracker: "Tracker",
                              domain: Dict[Text, Any], ) -> Dict[Text, Any]:
        if self.flag_phone_verifying:
            phone_numbers = self.information_extractor.get_phone_number_from_text(value)
            if len(phone_numbers) > 0:
                self.flag_phone_verifying = False
                return {"phone_number": value}
            else:
                return {"phone_number": None}
        if isinstance(value, str):
            phone_numbers = self.information_extractor.get_phone_number_from_text(value)
            if len(phone_numbers) > 0:
                return {"phone_number": phone_numbers[0]}
            else:
                dispatcher.utter_message(
                    text="Có vẻ như số điện thoại {} được bạn cung cấp không phải là một số điện thoại của Việt Nam!".format(
                        value))
                return {"phone_number": None}

    def validate_verify_info_contact(self, value: "Text", dispatcher: "CollectingDispatcher", tracker: "Tracker",
                                     domain: Dict[Text, Any], ) -> Dict[Text, Any]:
        if isinstance(value, str):
            intent = tracker.latest_message.get("intent", {}).get("name")
            if intent == 'affirm':
                return {"verify_info_contact": "True"}
            if value in ["True", "Đúng vậy", "Đúng rồi bạn"]:
                return {"verify_info_contact": "True"}

        return {"verify_info_contact": None, "user_name": None, "phone_number": None}

    def request_next_slot(
            self,
            dispatcher: "CollectingDispatcher",
            tracker: "Tracker",
            domain: Dict[Text, Any],
    ) -> Optional[List[EventType]]:
        """Request the next slot and utter template if needed,else return None"""
        for slot in self.required_slots(tracker):
            if self._should_request_slot(tracker, slot):
                print("$$$$$$$$$$$$$  Current SLOT : {} $$$$$$$$$$$$$$".format(slot))
                intent = tracker.latest_message.get("intent", {}).get("name")
                if intent == 'cancel':
                    dispatcher.utter_message(
                        "Dạ vâng! Bạn ngừng đặt phòng, bạn có cần Suni hỗ trợ thêm gì nữa không ạ?")
                    return self.deactivate()
                if slot == "destination":
                    dispatcher.utter_message(
                        template=f"utter_ask_destination"
                    )
                    dispatcher.utter_message(json_message=self.template_selector.get_template_destination())

                elif slot == "hotel_name":
                    dispatcher.utter_message(
                        template=f"utter_ask_hotel_name"
                    )
                    dispatcher.utter_message(
                        json_message=self.template_selector.get_template_hotel_name(tracker.get_slot("destination"))
                    )

                elif slot == "type_room":
                    dispatcher.utter_message(
                        template=f"utter_ask_type_room"
                    )
                    dispatcher.utter_message(
                        json_message=self.template_selector.get_template_type_room(tracker.get_slot("hotel_name"))
                    )

                elif slot == "verify_info_hotel":
                    text = "Mời bạn xác nhận thông tin phòng khách sạn:\nTên khách sạn : {hotel_name}\nĐịa chỉ : {destination}" \
                           "\nTừ ngày {from_date} đến ngày {to_date}\nLoại phòng : {type_room}" \
                           "\nSố lượng phòng : {count_room}".format(hotel_name=tracker.get_slot('hotel_name'),
                                                                    destination=tracker.get_slot('hotel_name'),
                                                                    from_date=tracker.get_slot('from_date'),
                                                                    to_date=tracker.get_slot('to_date'),
                                                                    type_room=tracker.get_slot('type_room'),
                                                                    count_room=tracker.get_slot('count_room'))

                    buttons = [{'title': 'Đúng vậy',
                                'payload': '/booking_room{"verify_info_hotel": "True"}'},
                               {'title': 'Đổi thông tin',
                                'payload': '/booking_room{"verify_info_hotel": "False"}'}]

                    dispatcher.utter_message(
                        text=text,
                        buttons=buttons
                    )

                elif slot == "verify_info_contact":

                    text = "Mời bạn xác nhận thông tin cá nhân:\nHọ và tên : {user_name}\nSố điện thọai: {phone_number}" \
                        .format(user_name=tracker.get_slot('user_name'),
                                phone_number=tracker.get_slot('phone_number'),
                                email=tracker.get_slot('email'))

                    buttons = [{'title': 'Đúng vậy',
                                'payload': '/booking_room{"verify_info_contact": "True"}'},
                               {'title': 'Đổi thông tin',
                                'payload': '/booking_room{"verify_info_contact": "False"}'}]

                    dispatcher.utter_message(
                        text=text,
                        buttons=buttons
                    )
                else:
                    dispatcher.utter_message(
                        template=f"utter_ask_{slot}", **tracker.slots,
                    )
                return [SlotSet(REQUESTED_SLOT, slot)]
        return None

    def send_room_code_by_sender_id(self):
        return "ABC123"

    def submit(
            self,
            dispatcher: "CollectingDispatcher",
            tracker: "Tracker",
            domain: Dict[Text, Any],
    ) -> List[EventType]:
        room_code = self.send_room_code_by_sender_id()
        text = "Cảm ơn bạn đã đặt phòng khách sạn với Suni.\n Mã đặt phòng của bạn là : {room_code}." \
               "\nBạn có muốn Suni tư vấn giúp bạn vấn đề gì nữa không ạ ?".format(room_code=room_code)
        response_messages = text.split("\n")
        for message in response_messages:
            dispatcher.utter_message(
                text=message,
            )
        self.sender_id = ""
        # self.data_loader.delete_image(self.sender_id)
        return [SlotSet("destination", None), SlotSet("hotel_name", None), SlotSet("from_date", None),
                SlotSet("to_date", None), SlotSet("type_room", None), SlotSet("count_room", None),
                SlotSet("verify_info_hotel", None), SlotSet("user_name", None), SlotSet("phone_number", None)
            , SlotSet("verify_info_contact", None)]


class AskDestinationInfoAction(Action):
    def name(self) -> Text:
        return "ask_destination_info_action"

    def run(
            self,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],
    ) -> List[EventType]:
        return []
