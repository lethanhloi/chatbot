from typing import Any, Text, Dict, List, Union, Optional
from rasa_sdk import Tracker
from rasa_sdk.events import EventType, SlotSet
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction, REQUESTED_SLOT

from application.service.service_extractor_ocr import *
from application.data.data_loader import *
from application.service.service_check_infor_checkin import *


class CheckinForm(FormAction):

    def __init__(self):
        self.sender_id = ""
        self.pass_card_id = False
        self.data_loader = DataLoader()
        self.ex = ServiceExtractorOCR()
        self.check_info = CheckInfoCheckin()

    def name(self) -> Text:
        return "checkin_form"

    @staticmethod
    def required_slots(tracker: "Tracker") -> List[Text]:
        return [
            "room_code",
            "frontend_id_card",
            "backend_id_card",
            "id_card",
            "user_name",
            "verify_info_id",
            "verify_info_name"
        ]

    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict[Text, Any]]]]:
        return {'room_code': [self.from_entity(entity="room_code"), self.from_text()],
                'frontend_id_card': [self.from_entity(entity="frontend_id_card"), self.from_text()],
                'backend_id_card': [self.from_entity(entity="backend_id_card"), self.from_text()],
                'user_name': [self.from_entity(entity="user_name"), self.from_text()],
                'id_card': [self.from_entity(entity="id_card"), self.from_text()],
                'verify_info_name': [self.from_entity(entity="verify_info_name"), self.from_text()],
                'verify_info_id': [self.from_entity(entity="verify_info_id"), self.from_text()]
                }

    def validate_room_code(self, value: "Text", dispatcher: "CollectingDispatcher", tracker: "Tracker",
                           domain: Dict[Text, Any],
                           ) -> Dict[Text, Any]:
        if isinstance(value, str):
            print("room code : " + value)
            if self.check_info.check_room_code(value):
                return {"room_code": value}
        return {"room_code": None}

    def validate_frontend_id_card(
            self,
            value: "Text",
            dispatcher: "CollectingDispatcher",
            tracker: "Tracker",
            domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        if isinstance(value, str):
            print("front end : " + str(value))
            self.sender_id = tracker.sender_id
            image_path = str(self.sender_id) + "_frontend"
            if not self.data_loader.download_image_url(value, image_path):
                dispatcher.utter_message(
                    text="Hình như có vấn đề trong quá trình nhập ảnh!"
                )
                return {"frontend_id_card": None}
            return {"frontend_id_card": value}
        return {"frontend_id_card": None}

    def validate_backend_id_card(
            self,
            value: "Text",
            dispatcher: "CollectingDispatcher",
            tracker: "Tracker",
            domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        if isinstance(value, str):
            print("back end : " + value)
            image_path = str(self.sender_id) + "_backend"
            if not self.data_loader.download_image_url(value, image_path):
                dispatcher.utter_message(
                    text="Hình như có vấn đề trong quá trình nhập ảnh!"
                )
                return {"backend_id_card": None}
            return {"backend_id_card": value}
        return {"backend_id_card": None}

    def validate_user_name(self, value: "Text", dispatcher: "CollectingDispatcher", tracker: "Tracker",
                           domain: Dict[Text, Any],
                           ) -> Dict[Text, Any]:
        if isinstance(value, str):
            return {"user_name": value}
        return {"user_name": None}

    def validate_id_card(self, value: "Text", dispatcher: "CollectingDispatcher", tracker: "Tracker",
                         domain: Dict[Text, Any],
                         ) -> Dict[Text, Any]:
        if isinstance(value, str):
            return {"id_card": value}
        return {"id_card": None}

    def validate_verify_info_id(self, value: "Text", dispatcher: "CollectingDispatcher", tracker: "Tracker",
                                domain: Dict[Text, Any]) -> Dict[Text, Any]:
        intent = tracker.latest_message.get("intent", {}).get("name")
        if intent == 'affirm':
            return {"verify_info_id": "True"}
        if value in ["True", "Đúng vậy", "Đúng rồi bạn"]:
            return {"verify_info_id": "True"}
        return {"verify_info_id": None, "id_card": None}

    def validate_verify_info_name(
            self,
            value: "Text",
            dispatcher: "CollectingDispatcher",
            tracker: "Tracker",
            domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        intent = tracker.latest_message.get("intent", {}).get("name")
        if intent == 'affirm':
            return {"verify_info_name": "True"}
        if value in ["True", "Đúng vậy", "Đúng rồi bạn"]:
            return {"verify_info_name": "True"}
        return {"verify_info_name": None, "user_name": None}

    def request_next_slot(
            self,
            dispatcher: "CollectingDispatcher",
            tracker: "Tracker",
            domain: Dict[Text, Any],
    ) -> Optional[List[EventType]]:
        """Request the next slot and utter template if needed,else return None"""
        for slot in self.required_slots(tracker):
            if self._should_request_slot(tracker, slot):
                intent = tracker.latest_message.get("intent", {}).get("name")
                if intent == 'cancel':
                    dispatcher.utter_message(
                        "Dạ vâng! Bạn không muốn tìm hiểu về dịch vụ của mình nữa, bạn có cần bot hỗ trợ thêm gì nữa không ạ?")
                    return self.deactivate()
                print("$$$$$$$$$$$$$  Current SLOT : {} $$$$$$$$$$$$$$".format(slot))

                if slot in ["backend_id_card"]:
                    if self.pass_card_id is False:
                        frontend_path = self.data_loader.load_image_frontend(self.sender_id)
                        id_card_infor = self.ex.orc_extract_upload_passport_id_card_upload(frontend_path, frontend_path,
                                                                                           frontend_path)
                        try:
                            if id_card_infor.type in ["tcc", "cmt", "passport"]:
                                if id_card_infor.id is not None and id_card_infor.name is not None:
                                    self.pass_card_id = True

                                    if id_card_infor.type == "passport":
                                        text = "Mời bạn xác nhận ID Passport của bạn: {id}".format(id=id_card_infor.id)
                                        buttons = [{'title': 'Đúng vậy',
                                                    'payload': '/checkin{"verify_info_id": "True"}'},
                                                   {'title': 'Đổi thông tin',
                                                    'payload': '/checkin{"verify_info_id": "False"}'}]

                                        response_messages = text.split("\n")
                                        for message in response_messages:
                                            dispatcher.utter_message(
                                                text=message,
                                                buttons=buttons
                                            )
                                        return [SlotSet("backend_id_card", ""),
                                                SlotSet("user_name", id_card_infor.name),
                                                SlotSet("id_card", id_card_infor.id),
                                                SlotSet(REQUESTED_SLOT, "verify_info_id")]

                                    elif id_card_infor.type in ["cmt", "tcc"]:
                                        dispatcher.utter_message(
                                            template=f"utter_ask_backend_id_card"
                                        )
                                        return [SlotSet("user_name", id_card_infor.name),
                                                SlotSet("id_card", id_card_infor.id),
                                                SlotSet(REQUESTED_SLOT, "backend_id_card")]

                            elif id_card_infor.type in ["matsautcc", "matsaucmt"]:
                                dispatcher.utter_message(
                                    text=f"Xin lỗi, ảnh bạn vừa nhập là ảnh mặt sau của chứng minh thư hoặc thẻ căn cước"
                                )
                                dispatcher.utter_message(
                                    template=f"utter_ask_frontend_id_card"
                                )
                                return [SlotSet("frontend_id_card", None),
                                        SlotSet(REQUESTED_SLOT, "frontend_id_card")]
                            else:
                                if id_card_infor.id is None or id_card_infor.name is None:
                                    text = "Xin lỗi bạn. Mình không trích xuất được thông tin trong ảnh của bạn.\nBạn vui lòng nhập" \
                                           " lại giúp mình ảnh chứng minh thư mặt trước hoặc ảnh passport của bạn với nhé"

                                    response_messages = text.split("\n")
                                    for message in response_messages:
                                        dispatcher.utter_message(
                                            text=message
                                        )

                                    return [SlotSet("user_name", None),
                                            SlotSet("frontend_id_card", None),
                                            SlotSet("backend_id_card", None),
                                            SlotSet(REQUESTED_SLOT, "frontend_id_card")]

                        except:
                            text = "Xin lỗi bạn. Mình không trích xuất được thông tin trong ảnh của bạn :(" \
                                   "\nBạn vui lòng nhập lại giúp mình với nhé"
                            response_messages = text.split("\n")
                            for message in response_messages:
                                dispatcher.utter_message(
                                    text=message
                                )

                            dispatcher.utter_message(
                                template=f"utter_ask_frontend_id_card"
                            )
                            return [SlotSet("user_name", None),
                                    SlotSet("frontend_id_card", None),
                                    SlotSet("backend_id_card", None),
                                    SlotSet(REQUESTED_SLOT, "frontend_id_card")]

                    if self.pass_card_id is True:
                        pass

                elif slot == "verify_info_id":
                    try:
                        backend_path = self.data_loader.load_image_backend(self.sender_id)
                        id_card_infor = self.ex.orc_extract_upload_passport_id_card_upload(backend_path, backend_path,
                                                                                           backend_path)

                        if id_card_infor.type in ["matsautcc", "matsaucmt"]:
                            id = tracker.get_slot("id_card")
                            text = "Mời bạn xác nhận Số chứng minh nhân dân : {id}".format(id=id)
                            buttons = [{'title': 'Đúng vậy',
                                        'payload': '/checkin{"verify_info_id": "True"}'},
                                       {'title': 'Đổi thông tin',
                                        'payload': '/checkin{"verify_info_id": "False"}'}]

                            response_messages = text.split("\n")
                            for message in response_messages:
                                dispatcher.utter_message(
                                    text=message,
                                    buttons=buttons
                                )
                            return [SlotSet(REQUESTED_SLOT, slot)]

                        else:
                            dispatcher.utter_message(
                                text=f"Xin lỗi, ảnh bạn vừa nhập không phải ảnh mặt sau của chứng minh thư hoặc thẻ căn cước"
                            )
                            dispatcher.utter_message(
                                template=f"utter_ask_backend_id_card"
                            )
                            return [SlotSet("backend_id_card", None),
                                    SlotSet(REQUESTED_SLOT, "backend_id_card")]

                    except Exception as e:
                        text = "Xin lỗi bạn. Mình không trích xuất được thông tin trong ảnh của bạn :(" \
                               "\nBạn vui lòng nhập lại giúp mình với nhé"
                        response_messages = text.split("\n")
                        for message in response_messages:
                            dispatcher.utter_message(
                                text=message
                            )

                        dispatcher.utter_message(
                            template=f"backend_id_card"
                        )
                        return [SlotSet("backend_id_card", None),
                                SlotSet(REQUESTED_SLOT, "backend_id_card")]

                elif slot == "verify_info_name":
                    name = tracker.get_slot("user_name")
                    text = "Mời bạn xác nhận Họ và tên : {name}".format(name=name)

                    buttons = [{'title': 'Đúng vậy',
                                'payload': '/checkin{"verify_info_name": "True"}'},
                               {'title': 'Đổi thông tin',
                                'payload': '/checkin{"verify_info_name": "False"}'}]

                    response_messages = text.split("\n")
                    for message in response_messages:
                        dispatcher.utter_message(
                            text=message,
                            buttons=buttons
                        )
                    return [SlotSet(REQUESTED_SLOT, slot)]
                else:
                    dispatcher.utter_message(
                        template=f"utter_ask_{slot}", **tracker.slots,
                    )
                return [SlotSet(REQUESTED_SLOT, slot)]
        return None

    def submit(
            self,
            dispatcher: "CollectingDispatcher",
            tracker: "Tracker",
            domain: Dict[Text, Any],
    ) -> List[EventType]:
        text = "Cảm ơn bạn đã cung cấp thông tin. Bên phía khách sạn sẽ check thông tin của bạn và liên hệ với " \
               "bạn sớm nhất.\nBạn có muốn Bot tư vấn giúp bạn vấn đề gì nữa không ạ ?"
        response_messages = text.split("\n")
        for message in response_messages:
            dispatcher.utter_message(
                text=message,
            )
        self.sender_id = ""
        self.pass_card_id = False
        # self.data_loader.delete_image(self.sender_id)
        return [SlotSet("user_name", None), SlotSet("frontend_id_card", None), SlotSet("backend_id_card", None),
                SlotSet("id_card", None), SlotSet("verify_info_id", None), SlotSet("verify_info_name", None),
                SlotSet("room_code", None)]
