import urllib.request
import requests
import os
import logging

logger = logging.getLogger(__name__)

class DataLoader:

    def __init__(self):
        self.folder_path = "application/data/storage"

    def download_image_url(self, url, image_file_name):
        try:
            path = self.folder_path + '/' + image_file_name + ".jpg"
            r = requests.get(url, allow_redirects=True)
            open(path, 'wb').write(r.content)
            return True
        except Exception as e:
            logger.error(e)
        return False

    def load_image_frontend_backend(self, sender_id):
        path_frontend = self.folder_path + '/' + sender_id + "_frontend.jpg"
        path_backend = self.folder_path + '/' + sender_id + "_backend.jpg"
        return path_frontend, path_backend

    def load_image_frontend(self, sender_id):
        path_frontend = self.folder_path + '/' + sender_id + "_frontend.jpg"
        return path_frontend

    def load_image_backend(self, sender_id):
        path_backend = self.folder_path + '/' + sender_id + "_backend.jpg"
        return path_backend

    def load_image_passport(self, sender_id):
        path_passport = self.folder_path + '/' + sender_id + "_passport.jpg"
        return path_passport

    def delete_image(self, sender_id):
        path_frontend = self.folder_path + '/' + sender_id + "_frontend.jpg"
        path_backend = self.folder_path + '/' + sender_id + "_backend.jpg"
        os.remove(path_frontend)
        os.remove(path_backend)

# if __name__ == '__main__':
#     data_loader = DataLoader()
#     url = "https://scontent.xx.fbcdn.net/v/t1.15752-9/117956041_2765170617095346_3818463477401289696_n.jpg?_nc_cat=102&_nc_sid=b96e70&_nc_ohc=m208hPHC64gAX_nbisN&_nc_ad=z-m&_nc_cid=0&_nc_ht=scontent.xx&oh=1690709f1d4cd2446a8f17d47496bbbb&oe=5F624037"
#     data_loader.download_image_url(url,'loi')
#     # import requests
#
#     # url = 'https://www.facebook.com/favicon.ico'
#     # r = requests.get(url, allow_redirects=True)
#     #
# open('facebook.jpg', 'wb').write(r.content)
