class MessageFacebook(object):

    sender_id = ""
    recipient_id = ""
    timestamp = ""
    message = {}

    def __init__(self, sender_id, recipient_id, timestamp, message):
        self.sender_id = sender_id
        self.recipient_id = recipient_id
        self.timestamp = timestamp
        self.message = message


