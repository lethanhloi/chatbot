class IdCardObject(object):
    type = ""
    address = ""
    born = ""
    name = ""
    id = ""
    ethnicity = ""
    sex = ""

    def __init__(self, type, address, born, name, id, ethnicity, sex):
        self.type = type
        self.address = address
        self.born = born
        self.name = name
        self.id = id
        self.ethnicity = ethnicity
        self.sex = sex

    def information(self):
        result = {"type : {type}, address : {address},born: {born},name:{name},id: {id},ethnicity: {ethnicity}, sex: {sex}"
                      .format(type=self.type, address=self.address, born=self.born, name=self.name, id=self.id,ethnicity=self.ethnicity, sex = self.sex)}

        return result
