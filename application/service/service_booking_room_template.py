import random
import json
import logging

BOOKING_ROOM_TEMPLATE_PATH = "application/rule_text/templates/booking_room_template.json"


class TemplateSelector(object):
    def __init__(self):
        self.data = []
        self.destinations = []
        try:
            with open(BOOKING_ROOM_TEMPLATE_PATH) as f:
                self.data = json.load(f)
            self.destinations = self.data['destination'].keys()
        except Exception as e:
            logging.error(e)

    def get_template_destination(self):
        elements = []
        elements_selected = random.sample(self.destinations,
                                          9 if len(self.destinations) > 9 else len(self.destinations))

        for element in elements_selected:
            elements.append(self.data["destination"][element])

        templates = {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": elements
                }
            }
        }
        return templates

    def get_template_hotel_name(self, destination):
        elements = []
        if destination not in self.destinations:
            return None

        elements_selected = random.sample(self.data['hotel_name'][destination].keys(),
                                          9 if len(self.data['hotel_name'][destination].keys()) > 9 else len(
                                              self.data['hotel_name'][destination].keys()))

        for element in elements_selected:
            elements.append(self.data['hotel_name'][destination][element])

        templates = {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": elements
                }
            }
        }
        return templates

    def get_template_type_room(self, hotel_name):
        elements = []
        list_hotels = self.data['type_room'].keys()
        if hotel_name not in list_hotels:
            return None

        elements_selected = random.sample(self.data['type_room'][hotel_name].keys(),
                                          9 if len(self.data['type_room'][hotel_name].keys()) > 9 else len(
                                              self.data['type_room'][hotel_name].keys()))

        for element in elements_selected:
            elements.append(self.data['type_room'][hotel_name][element])

        templates = {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": elements
                }
            }
        }

        return templates
