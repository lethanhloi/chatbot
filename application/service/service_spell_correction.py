import json, logging

MAPPING_SPELL_CORRECTION_PATH = "application/rule_text/mapping/mapping_spell_correction.json"
BOOKING_ROOM_TEMPLATE_PATH = "application/rule_text/response/booking_room_template.json"


class VerifyBookingRoom(object):
    def __init__(self):
        self.data = []
        self.destinations = []
        try:
            with open(BOOKING_ROOM_TEMPLATE_PATH) as f:
                self.data = json.load(f)
            self.destinations = self.data['destination'].keys()
        except Exception as e:
            logging.error(e)

    def verify_destination_names(self, string) -> str:
        try:

            return ""
        except Exception as e:
            logging.error(e)
            return ""


class SpellCorrection(object):

    def __init__(self):
        self.mapping_destination_name = []
        try:
            with open(MAPPING_SPELL_CORRECTION_PATH) as f:
                self.data = json.load(f)
            self.mapping_destination_name = self.data['destination'].keys()
        except Exception as e:
            logging.error(e)

    def standardized_destination(self, destination_string):
        pass
