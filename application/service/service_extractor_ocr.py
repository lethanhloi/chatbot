import requests
import application.settings as config
from application.settings import *
from application.entity.object_id_card import *

mattruoc_url = 'https://i.pinimg.com/originals/2f/7d/8b/2f7d8b56ed667f5e3f35160378596a10.jpg'
matsau_url = 'https://cms.luatvietnam.vn/uploaded/Images/Original/2019/10/25/tay-not-ruoi-xoa-seo-co-phai-lam-lai-chung-minh-nhan-dan_2510140542.jpg'

mattruoc = 'https://scontent.xx.fbcdn.net/v/t1.15752-9/117956041_2765170617095346_3818463477401289696_n.jpg?_nc_cat=102&_nc_sid=b96e70&_nc_ohc=q_IEE89PWMcAX_IKLIX&_nc_ad=z-m&_nc_cid=0&_nc_ht=scontent.xx&oh=c8e0986a2bef7261d367d3d044713260&oe=5F6634B7'
matsau = 'https://scontent.xx.fbcdn.net/v/t1.15752-9/117767631_303063407637802_1570018709396429239_n.jpg?_nc_cat=103&_nc_sid=b96e70&_nc_ohc=ca0_HC3LoacAX_aYbi2&_nc_ad=z-m&_nc_cid=0&_nc_ht=scontent.xx&oh=99c80e1281a5b567e78d3f02f022f150&oe=5F65723A'

local3 = "/home/loilt/Desktop/chatbot/chatbot_khachsan/application/data/storage/tccsau.jpg"


class ServiceExtractorOCR:

    def __init__(self):
        self.api_key_url = config.API_KEY_URL
        self.api_secret_url = config.API_SECRET_URL
        self.api_key_upload = config.API_KEY_UPLOAD
        self.api_secret_upload = config.API_SECRET_UPLOAD
        self.api_get_info_url = config.API_GET_INFOR_ID_CARD_URL
        self.api_get_info_upload = config.API_GET_INFOR_ID_CARD_UPLOAD
        self.api_get_info_upload_front = config.API_GET_INFOR_ID_CARD_UPLOAD_FRONT
        self.api_get_info_upload_back = config.API_GET_INFOR_ID_CARD_UPLOAD_BACK
        self.api_get_info_upload_passport_front_back = config.API_GEt_INFOR_ID_CARD_PASSPORT_UPLOAD

    def orc_extractor_url(self, frontend_url, backend_url):
        response = requests.get(
            self.api_get_info_url + "?mattruoc=%s&matsau=%s" % (frontend_url, backend_url),
            auth=(self.api_key_url, self.api_secret_url)).json()
        try:
            id = response['data']['mattruoc']['data']['id']
            name = response['data']['mattruoc']['data']['name']
            address = response['data']['mattruoc']['data']['address']
            born = response['data']['mattruoc']['data']['born']
            ethnicity = response['data']['matsau']['data']['dantoc']

            id_card = IdCardObject("", address, born, name, id, ethnicity, "")
            return id_card
        except:
            print("error")
        return None

    def orc_extractor_upload(self, frontend_path, backend_path):
        response = requests.post(
            self.api_get_info_upload,
            auth=(self.api_key_upload, self.api_secret_upload),
            files={'mattruoc': open(frontend_path, 'rb'), 'matsau': open(backend_path, 'rb')}).json()
        try:
            id = response['data']['mattruoc']['data']['id']
            name = response['data']['mattruoc']['data']['name']
            address = response['data']['mattruoc']['data']['address']
            born = response['data']['mattruoc']['data']['born']
            ethnicity = response['data']['matsau']['data']['dantoc']

            id_card = IdCardObject("", address, born, name, id, ethnicity, "")
            return id_card
        except:
            print("error")
        return None

    def orc_extractor_upload_frontend(self, frontend_path):
        response = requests.post(
            self.api_get_info_upload,
            auth=(self.api_key_upload, self.api_secret_upload),
            files={'image': open(frontend_path, 'rb')}).json()
        print(response)
        try:
            id = response['data']['mattruoc']['data']['id']
            name = response['data']['mattruoc']['data']['name']
            address = response['data']['mattruoc']['data']['address']
            born = response['data']['mattruoc']['data']['born']
            id_card = IdCardObject("", address, born, name, id, "", "")
            return id_card
        except:
            print("error")
        return None

    def orc_extractor_upload_backend(self, backend_path):
        """
        :param backend_path:
        :return:
        """
        response = requests.post(
            self.api_get_info_upload,
            auth=(self.api_key_upload, self.api_secret_upload),
            files={'image': open(backend_path, 'rb')}).json()
        print(response)
        try:
            ethnicity = response['data']['matsau']['data']['dantoc']
            id_card = IdCardObject("", "", "", "", "", ethnicity, "")
            return id_card
        except:
            print("error")
        return None

    def orc_extract_upload_passport_id_card_upload(self, front_end_path, back_end_path, passport_path):
        """
        :param front_end_path:
        :param back_end_path:
        :param passport_path:
        :return:
        """
        for path in [front_end_path, back_end_path, passport_path]:
            response = requests.post(
                self.api_get_info_upload_passport_front_back,
                auth=(self.api_key_upload, self.api_secret_upload),
                files={'image': open(path, 'rb')}).json()
            print(response)
            try:
                type = response['type']
                id = None
                name = None
                address = None
                born = None
                ethnicity = None
                sex = None
                if type == "cmt":
                    id = response['data']['id']
                    name = response['data']['name']
                    address = response['data']['address']
                    born = response['data']['born']

                elif type == "matsaucmt":
                    pass

                elif type == "passport":
                    id = response['data']['id']
                    name = response['data']['surname'] + " " + response['data']['given_name']
                    address = response['data']['country']
                    born = response['data']['born']
                    sex = response['data']['sex']

                elif type == "tcc":
                    id = response['data']['id']
                    name = response['data']['name']
                    address = response['data']['country']
                    born = response['data']['born']
                    sex = response['data']['sex']

                elif type == "matsautcc":
                    pass
                id_card = IdCardObject(type, address, born, name, id, ethnicity, sex)
                print(id_card.information())
                return id_card

            except Exception as e:
                print("error")

        return None


# if __name__ == '__main__':
#     ex = ServiceExtractorOCR()
#     # ex.orc_extractor_url(local, local)
#     # ex.orc_extractor_upload(local, local)
#     ex.orc_extract_upload_passport_id_card_upload(local3, local3, local3)
