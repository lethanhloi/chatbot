class CheckInfoCheckin:

    @staticmethod
    def check_room_code(room_code: str) -> bool:
        """
        :param room_code:
        :return:
        """
        return True

    @staticmethod
    def check_frontend_id_card(frontend_path, ex):
        """
        :type frontend_path: object
        """
        return ex.orc_extractor_upload_frontend(frontend_path)

    @staticmethod
    def check_backend_id_card(backend_path, ex):
        return ex.orc_extractor_upload_backend(backend_path)
