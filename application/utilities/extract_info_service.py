import re
from datetime import datetime
import logging

REGEX_PHONE_NUMBER = r"((?:\+?84|0)(\s)?)([\.\-])?(\d{3})(\s*)?([[\.\-])?(\d{3})(\s*)?([[\.\-])?(\d{3})\b"
regex_email = r"[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}"
regex_contact_mail = ["email", "mail", "thư điện tử"]
regex_contact_phone = ["số điện thoại", "điện thoại", "sđt", "sdt", "phone"]


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


class InformationExtractor(object):

    def __init__(self):
        pass

    def get_phone_number_from_text(self, text: str) -> list:
        """
        :param text:
        :return: find phone number of Viet Nam in text param
        """
        phones = set()
        match = re.findall(REGEX_PHONE_NUMBER, text)
        if match:
            for item in match:
                number = ''
                for i in item:
                    number += i
                phones.add(number)
        phones = list(phones)
        return phones

    def get_email_from_text(self, text: str) -> list:
        """
        :type text: object
        """
        return re.findall(regex_email, text)

    def check_date_format(self, date_string):
        list_date_format = ["%d/%m/%Y"]
        result = False
        for format in list_date_format:
            try:
                datetime.strptime(date_string, format)
                return True
            except:
                result = False

        return result

    def get_contact_channel_from_text(self, text: str) -> list:
        list_result = []
        if len(self.get_email_from_text(text)):
            list_result.append("email")
        if len(self.get_phone_number_from_text(text)):
            list_result.append("số điện thoại")
        text = text.lower()
        if "email" not in list_result:
            for sym in regex_contact_mail:
                if sym in text:
                    list_result.append("email")
                    break
        if "số điện thoại" not in list_result:
            for sym in regex_contact_phone:
                if sym in text:
                    list_result.append("số điện thoại")
                    break
        return list_result


# if __name__ == '__main__':
#     ie = InformationExtractor()
#     ie.get_phone_number_from_text("000000000")
