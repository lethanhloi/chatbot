# Hướng dẫn cài đặt và sử dụng
Đây là phần code cho chatbot khách sạn

## Prerequisite

- Rasa 1.10.0
- Rasa x

Cài đặt Rasa và Rasa x xem tại [đây](https://github.com/RasaHQ/rasa-demo) và tại [đây](https://towardsdatascience.com/create-chatbot-using-rasa-part-1-67f68e89ddad)
        
- Spacy

        pip install spacy
        pip install https://github.com/trungtv/vi_spacy/raw/master/packages/vi_spacy_model-0.2.1/dist/vi_spacy_model-0.2.1.tar.gz
        
- Pyvi

        pip install pyvi


## Training :

- Chạy các action:
        
        rasa run actions [-p]
    
- Huấn luyện phần Core và NLU
    
        rasa train

- Chạy duckling entity extractor < phần này nếu sử dụng Duckling mới cần chạy >:

        docker run -p 8000:8000 rasa/duckling

## Tests

- Chạy shell (dùng --debug với trường hợp muốn debug)

        rasa shell [--debug]

![Alt text](readme_images/rasa_shell_demo.png?raw=true "Rasa shell")

- Hoặc Chạy với giao diện
        
        rasa x

![Alt text](readme_images/rasa_x_demo.png?raw=true "Rasa x")


# Các thành phần trong project
## Các thành phần chính :
1. Các intent, entity,form actions được định nghĩa trong file doamin.yml
2. Các mô hình sử dụng cho phần NLU, Core được định nghĩa trong file config.yml
3. Dữ liệu train được lưu trong thư mục /data , bao gồm : 
    - Các kịch bản : trong file /core/stories.md
    - Các dữ liệu train nlu : trong folder /nlu
    
## Thư mục /actions : 
> Các actions của bot khi gặp được các intent (mục đích) được viết lại các hàm xử lý trong folder này.
- Các actions được custom lại bao gồm : 
    1. Default (trong file normal_actions): 
        - ActionSlotReset
        - ActionProductReset 
    
    2. Override (trong file override_actions) : 
        - ActionDefaultAskAffirmation
        - ActionDefaultFallback
        - ActionDefaultAskRephrase
    
    3. Các actions yêu cầu nhập theo form, định nghĩa các thành phần trong form (trong file product_actions, support_actions) 
    
## Thư mục /applications 
- Phần này lưu các service, lưu trữ, tiện ích 

