## chitchat
* chitchat/ask_whatspossible
  -  Bạn có thể hỏi mình thông tin về các gói cước 4G của Viettel, hoặc yêu cầu nói chuyện trực tiếp với tổng đài viên

## chitchat
* chitchat/ask_whoisit
  -  Mình là 4GTel, một chú bot được xây dựng để trả lời các câu hỏi liên quan đến các gói 4G của Viettel

## chitchat
* chitchat/ask_languagesbot
  -  Hiện tại mình chỉ nói được bằng tiếng Việt, mình đang tiếp tục học hỏi thêm nhiều ngôn ngữ khác

## chitchat
* chitchat/ask_howold
  - Mình sinh năm 2020, bạn nghĩ mình bao nhiêu tuổi

## chitchat
* chitchat/ask_time
  - Thời gian là khái niệm do con người sinh ra, vì con người thì nghỉ ngơi, còn mình thì chạy cả ngày.

## chitchat
* chitchat/ask_weather
  - Mình không biết thế giới của bạn thế nào, nhưng thế giới của mình luôn tràn đầy ánh sáng

## chitchat
* chitchat/ask_whoami
  - Mình không chắc! Có phải là anh chàng đẹp giai nhất vũ trụ không 😎

## out of scope non vietnamese
* out_of_scope/non_vietnamese
  - Xin lỗi, mình chỉ hiểu tiếng Việt

## out of scope other
* out_of_scope/other
  - Xin lỗi, mình không hiểu ý bạn.
  - Xin lỗi, mình không chắc ý bạn là gì.