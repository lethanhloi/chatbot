## intent:chitchat/ask_whatspossible
- Bạn có thể làm được gì
- Bot có thể làm gì
- Bạn có thể giúp gì được cho tôi
- Khả năng của bạn là gì
- Tôi có thể làm được gì ở đây
- Làm ơn giúp mình
- Tôi có thể hỏi bạn những gì
- Chúng ta sẽ nói về cái gì
- Bot làm được gì cho tôi
- Tôi phải làm gì ở đây
- bạn có gì
- kể cho mình về những gì bạn có đi
- tôi phải hỏi bạn cái gì
- tao có thể hỏi gì ở đây
- bây giờ tao phải làm gì
- bạn có thể làm gì

## intent:chitchat/ask_whoisit
- Bạn tên là gì
- Bạn là bot à?
- Bạn là người hay bot?
- Tôi đang nói chuyện với người hay bot vậy?
- Bạn là con bot do người ta lập trình phải không?
- Tên bạn là gì
- Tên chú mày là gì
- Bạn là ai
- Nói với tôi một số điều về bạn
- Tôi đang nói chuyện với ai đây
- Ai đang chat với mình vậy
- bạn là ai thế
- Bạn do ai tạo ra
- Bot là của công ty nào nhỉ
- Ai là bố của bot
- Ai là mẹ của bạn
- cháu ơi cho bà hỏi, cháu con nhà ai
- chú mày là con của ai
- ai xây dựng bot vậy
- bạn do ai lập trình ra thế
- bạn là của Vccorp à
- Bên bạn thuộc vccorp à?
- mi là cái chi?

## intent:chitchat/ask_languagesbot
- Bạn có thể nói bằng những thứ tiếng nào
- Mày nói được ngôn ngữ khác không
- Chat bằng tiếng Anh được không bạn
- Nói vài câu tiếng Pháp nghe chơi
- Bot có hỗ trợ tiếng anh không nhỉ
- Cho mình hỏi bằng tiếng Trung được không
- Bạn có thể nói bằng ngôn ngữ nào
- Bạn có phải người Đức không
- Cho hỏi câu bằng tiếng Nhật cái
- Nói tôi nghe vài câu tiếng Úc xem

## intent:chitchat/ask_howold
- Bạn mấy tuổi rồi
- Chú bao nhiêu tuổi rồi
- Mày trẻ hay là già
- Bot sinh năm bao nhiêu
- Tuổi của chú là bao nhiêu
- bot chắc còn trẻ lắm
- bot già rồi nhỉ
- bạn bao nhiêu tuổi thế
- bạn sinh lúc nào
- Năm nay là bạn bao nhiêu tuổi

## intent:chitchat/ask_time
- Mấy giờ rồi bạn
- Ở Hà Nội đang là mấy giờ
- Đang là mấy giờ ở Nhật Bản nhỉ
- Ở Mỹ là mấy giờ rồi nhỉ
- Bây giờ là mấy giờ rồi
- Bạn có biết bây giờ là mấy giờ không
- Bạn có đồng hồ ở đó không, cho hỏi mấy giờ rồi
- cho hỏi mấy giờ rồi
- chú cho anh hỏi bây giờ là mấy giờ
- mấy giờ rồi bot

## intent:chitchat/ask_weather
- thời tiết thế nào bot
- hôm nay trời nắng không nhỉ
- chắc nay trời mưa bão bùng luôn
- ngày mai thời tiết thế nào bot
- cho hỏi thời tiết ba ngày tới
- thời tiết trong thế giới của bot thế nào
- nhiệt độ hôm nay là bao nhiêu nhỉ
- Trời hôm nay nóng không bot
- Ngày mai thời tiết ở Hà Nội thế nào
- ở Đà Nẵng có nóng không bot
- thời tiết hôm nay thế nào

## intent:chitchat/ask_whoami
- bạn biết tôi là ai không
- bot có biết mình là ai không
- mày biết tao là ai không
- chú có biết ai là Lê Thành Lợi không
- bot biết Trần Lương Nguyên là ông nào không
- chú có biết anh là ai không
- mày biết Phạm Thanh Bình là bà nào không
- bạn có biết Tăng Quang Huy là ai không
- mày quen biết Steve Job không
- bạn biết ông Quân không
- bạn có biết anh Sỹ không
- bạn biết bà Trang không
- mày có quen lão Kiên không
- chú quen biết bà Yến không
- bot có biết sếp Vĩ không
- chú có quen ku Đức không
- biết ông Việt là ai không
- có biết lão Tài không bot
- chú biết Hoàng Văn Minh không bot
- chú có biết sếp của VCCorp không
- chú có quen ku Diện không
- biết bác Bộ không bot
- biết ai tên là Tùng không bot
- Chú mày biết ku Trần Văn Hiếu không nhỉ
- biết ai tên là Bill Gate chưa
- biết anh Hiệp là ai không
- bot có biết anh Hoàng Anh chưa
- có biết Ngọc Anh là ku nào không
- chú biết Elon Musk chứ nhỉ
- mày biết Hoàng là thằng nào không
- ai là người đầu tiên bước chân lên Mặt Trăng
- ai là tổng thống Hoa Kỳ
- ai là người phát minh ra máy hơi nước
- bạn có biết người nào đã đặt nền móng cho loài người không
