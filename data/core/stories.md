## happy path
* greet
  - utter_greet
  - action_slot_reset
* mood_great
  - utter_happy

## sad path 1
* greet
  - utter_greet
  - action_slot_reset
* mood_unhappy
  - utter_unhappy
  - utter_what_can_i_do
  - utter_support_direction
* affirm
  - utter_happy

## sad path 2
* greet
  - utter_greet
  - action_slot_reset
* mood_unhappy
  - utter_unhappy
  - utter_what_can_i_do
  - utter_support_direction

## deny
* deny
  - utter_anything_else

## greet + affirm
* greet
  - utter_greet
  - action_slot_reset
* affirm
  - utter_happy

## affirm
* affirm
  - utter_happy

## say goodbye
* goodbye
  - utter_goodbye
  - action_slot_reset

## cancel
* cancel
  - utter_happy
  - action_slot_reset

## thanks
* thanks
  - utter_thanks
  
## checkin
* checkin
  - checkin_form
  - form{"name": "checkin_form"}
  - form{"name": null}

##direct_counseling
* direct_counseling
  - utter_direct_counseling


## booking_room
* booking_room
  - booking_room_form
  - form{"name": "booking_room_form"}
  - form{"name": null}